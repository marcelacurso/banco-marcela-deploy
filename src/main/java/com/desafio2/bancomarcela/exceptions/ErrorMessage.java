package com.desafio2.bancomarcela.exceptions;

public class ErrorMessage {
    private String message;
    private String detail;
    private String code;
    private String  path;

    public String getMessage() {
        return message;
    }

    public String getDetail() {
        return detail;
    }

    public String getCode() {
        return code;
    }

    public String getPath() {
        return path;
    }

    public ErrorMessage(String message, String detail, String code, String path) {
        this.message = message;
        this.detail = detail;
        this.code = code;
        this.path = path;
    }
    public ErrorMessage() {}
}
