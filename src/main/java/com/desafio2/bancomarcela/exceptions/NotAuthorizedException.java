package com.desafio2.bancomarcela.exceptions;

public class NotAuthorizedException extends RuntimeException {
    public NotAuthorizedException(String message){
        super(message);
    }
}
