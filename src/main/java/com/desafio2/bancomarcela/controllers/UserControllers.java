package com.desafio2.bancomarcela.controllers;

import com.desafio2.bancomarcela.models.entities.AccountEntity;
import com.desafio2.bancomarcela.services.UsersService;
import com.desafio2.bancomarcela.models.entities.UserEntity;
import com.desafio2.bancomarcela.models.repositories.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")

public class UserControllers {
    private final UsersService usersService;

    public UserControllers(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/{userdni}")
    public UserEntity getUserDni(@PathVariable String userdni){
        return usersService.findByUserDni(userdni);
    }




    @PostMapping("/newUser")
    public UserEntity newUsers(@RequestBody UserEntity newUser){
        return usersService.newUsers(newUser);
    }

    /*@GetMapping("/{dni}")
    public List<UserEntity> getUserDni(@PathVariable String dni) {
        return userRepository.findByDni(dni);

    }

    @PostMapping("/new")
    public UserEntity newUser(@RequestBody UserEntity userEntity) {
        return userRepository.save(userEntity);

    }*/
}
