package com.desafio2.bancomarcela.controllers;

import com.desafio2.bancomarcela.models.dto.NewTransactionDTO;
import com.desafio2.bancomarcela.models.dto.TransactionDTO;
import com.desafio2.bancomarcela.services.TransactionsService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transactions")

public class TransactionsControllers {


    private final TransactionsService transactionsService;

    public TransactionsControllers(TransactionsService transactionsService){

        this.transactionsService = transactionsService;
    }

    @GetMapping("/all/{accountNumber}")
    public ResponseEntity<TransactionDTO> getTransactionForAccount(@PathVariable Long accountNumber){
        return new ResponseEntity(transactionsService.getTransactions(accountNumber), HttpStatus.NOT_FOUND);
    }

    @GetMapping("/{transactionId}")
    public ResponseEntity<TransactionDTO> getTransaction(@PathVariable Long transactionId){
        return new ResponseEntity(transactionsService.getTransaction(transactionId), HttpStatus.NOT_FOUND);
    }

    @PostMapping("/newTransactionDTO")
    public ResponseEntity<String> newTransaction(@RequestBody NewTransactionDTO newTransactionDTO){
        return transactionsService.newTransaction(newTransactionDTO);
    }
}






