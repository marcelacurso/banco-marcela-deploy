package com.desafio2.bancomarcela.controllers;

import com.desafio2.bancomarcela.models.entities.AccountEntity;
import com.desafio2.bancomarcela.models.repositories.AccountRepository;
import com.desafio2.bancomarcela.services.AccountsService;
import com.desafio2.bancomarcela.services.TransactionsService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")

public class AccountControllers {

    /*private final AccountRepository accountRepository;

    public AccountControllers(AccountRepository accountRepository)
    {
        this.accountRepository = accountRepository;
    }

    @GetMapping("/{userid}")
    public List<AccountEntity> getUserAccounts(@PathVariable String userid){
        return accountRepository.findAllByUserid(userid);
    }

    @PostMapping("/new")
    public AccountEntity newAccount(@RequestBody AccountEntity accountEntity){
        return accountRepository.save(accountEntity);
    }*/

    private final AccountsService accountsService;

    public AccountControllers(AccountsService accountsService){

        this.accountsService = accountsService;
    }

    @GetMapping("/{userid}")
    public List<AccountEntity> getUserAccounts(@PathVariable String userid){
        return accountsService.findAllByUserid(userid);
    }

    @PostMapping("/new")
    public AccountEntity newAccount(@RequestBody AccountEntity accountEntity){
        return accountsService.save(accountEntity);
    }





}
