package com.desafio2.bancomarcela.services;

import com.desafio2.bancomarcela.exceptions.NotFoundException;
import com.desafio2.bancomarcela.models.dto.TransactionDetailedDTO;
import com.desafio2.bancomarcela.models.entities.AccountEntity;
import com.desafio2.bancomarcela.models.entities.UserEntity;
import com.desafio2.bancomarcela.models.mappers.TransactionMapper;
import com.desafio2.bancomarcela.models.repositories.AccountRepository;
import com.desafio2.bancomarcela.models.repositories.TransactionsRepository;
import com.desafio2.bancomarcela.models.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class UsersService {
    private final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private final UserRepository usersRepository;
    private final TransactionMapper transactionMapper;

    public UsersService (TransactionsRepository transactionsRepository, AccountRepository accountRepository, UserRepository usersRepository,
                           TransactionMapper transactionMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.usersRepository = usersRepository;
        this.transactionMapper = transactionMapper;
    }

    public UserEntity findByUserDni(String userDni) {
        UserEntity userEntity = usersRepository.findByDni(userDni);
        if (Objects.isNull(userEntity)) {
            throw new NotFoundException("no existe el dni en la base");
        }

        return userEntity;
    }



    public UserEntity newUsers(UserEntity newUser) {

        UserEntity usersEntityBD = usersRepository.findByDni(newUser.getDni());

        if (newUser.getDni().equals(usersEntityBD.getDni()))
        {
            throw new NotFoundException("El dni ya existe");
        }

        return usersRepository.save(newUser);

    }

}
