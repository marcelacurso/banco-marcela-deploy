package com.desafio2.bancomarcela.services;

import com.desafio2.bancomarcela.exceptions.BadRequestException;
import com.desafio2.bancomarcela.exceptions.NotFoundException;
import com.desafio2.bancomarcela.models.dto.*;
import com.desafio2.bancomarcela.models.entities.AccountEntity;
import com.desafio2.bancomarcela.models.entities.TransactionsEntity;
import com.desafio2.bancomarcela.models.entities.UserEntity;
import com.desafio2.bancomarcela.models.mappers.TransactionMapper;
import com.desafio2.bancomarcela.models.repositories.AccountRepository;
import com.desafio2.bancomarcela.models.repositories.TransactionsRepository;
import com.desafio2.bancomarcela.models.repositories.UserRepository;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class TransactionsService {

    private final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private final UserRepository usersRepository;
    private final TransactionMapper transactionMapper;

    public TransactionsService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UserRepository usersRepository,
                               TransactionMapper transactionMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.usersRepository = usersRepository;
        this.transactionMapper = transactionMapper;
    }

    //1° punto del desafió
    public TransactionsDTO getTransactions(Long accountNumber) {

        List<TransactionsEntity> transactionsEntities = transactionsRepository.findAllByOriginOrDestination(accountNumber.toString(), accountNumber.toString());
        List<TransactionDTO> transactionDTOList = new ArrayList<>();

        for (TransactionsEntity transactionsEntity : transactionsEntities) {

            AccountEntity toAccountEntity = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getDestination()));

            UserEntity toUserEntity = usersRepository.findByDni(toAccountEntity.getUserid());
            String transactionType;

            if (transactionsEntity.getOrigin().equals(accountNumber.toString())) {
                transactionType = "Gasto";
            } else {
                transactionType = "Ingreso";
            }

            transactionDTOList.add(
                    new TransactionDTO(transactionsEntity.getDate(),
                            transactionsEntity.getDescription(),
                            transactionsEntity.getAmount(),
                            transactionsEntity.getCurrency().toString(),
                            transactionsEntity.getOrigin(),
                            transactionsEntity.getDestination(),
                            transactionType)
            );

        }
        BigDecimal balance = BigDecimal.ZERO;
        for (TransactionDTO transactionDTO : transactionDTOList) {
            if (transactionDTO.getType().equals("Gasto")) {
                balance = balance.subtract(transactionDTO.getAmount());
            } else {
                balance = balance.add(transactionDTO.getAmount());
            }
        }

        return new TransactionsDTO(transactionDTOList, balance);


    }

    // 2° punto del desafió
    public TransactionDetailedDTO getTransaction(Long transactionId) {

        Optional<TransactionsEntity> transactionsEntityOptional = transactionsRepository.findById(transactionId);
        if (transactionsEntityOptional.isPresent()) {

            TransactionsEntity transactionsEntity = transactionsEntityOptional.get();
            AccountEntity toAccountEntity = accountRepository.findByNumber(Long.parseLong(transactionsEntity.getDestination()));
            UserEntity toUserEntity = usersRepository.findByDni(toAccountEntity.getUserid());
            // transactionDetailedDTO.add(
            AccountDto to = new AccountDto(toUserEntity.getFirtName(), toUserEntity.getLastName(), toAccountEntity.getCbu());
            AccountDto from = new AccountDto(toUserEntity.getFirtName(), toUserEntity.getLastName(), toAccountEntity.getCbu());


            TransactionDetailedDTO transactionDetailedDTO = new TransactionDetailedDTO(
                    transactionsEntity.getDescription(),
                    transactionsEntity.getAmount(),
                    transactionsEntity.getCurrency(),
                    to,
                    from,
                    transactionsEntity.getDate()
            );

            return transactionDetailedDTO;

        } else { //return null;
            throw new NotFoundException("la transaction esta vacía");
        }
    }



//3°  punto del desafió

    public ResponseEntity<String> newTransaction(NewTransactionDTO newTransactionDTO) {
        AccountEntity toAccountEntity = accountRepository.findByNumber(Long.parseLong(newTransactionDTO.getTo()));

        //validar parametros
        if (newTransactionDTO.getFrom().equals(newTransactionDTO.getTo())) {
            throw  new BadRequestException("Error al crear la transaction, From y To no pueden ser iguales");
        }
        if (Objects.isNull(toAccountEntity)) {
            throw new BadRequestException("La cuenta destino no existe");
        }
        if (newTransactionDTO.getAmount().intValue() <= 0) {
            throw new BadRequestException("El monto es cero o negativo destino no existe");
        }

        try {
            //no se dbe llamar igual
            TransactionsEntity transactionsEntity = transactionMapper.mapTransactionDtoToTransactionEntity(newTransactionDTO);
            transactionsRepository.save(transactionsEntity);
            return new ResponseEntity<>( "Transaccion creada", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Algo salió mal", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    };


    public ResponseEntity<String> createTransaction(NewTransactionDTO newTransactionDTO){
        try {
            //no se dbe llamar igual
            TransactionsEntity transactionsEntity = transactionMapper.mapTransactionDtoToTransactionEntity(newTransactionDTO);
            transactionsRepository.save(transactionsEntity);
            return new ResponseEntity<>( "Transaccion creada", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Algo salió mal", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

