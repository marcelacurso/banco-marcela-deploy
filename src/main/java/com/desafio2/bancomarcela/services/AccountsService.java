package com.desafio2.bancomarcela.services;

import com.desafio2.bancomarcela.exceptions.NotFoundException;
import com.desafio2.bancomarcela.models.dto.AccountDto;
import com.desafio2.bancomarcela.models.dto.TransactionDetailedDTO;
import com.desafio2.bancomarcela.models.entities.AccountEntity;
import com.desafio2.bancomarcela.models.entities.TransactionsEntity;
import com.desafio2.bancomarcela.models.entities.UserEntity;
import com.desafio2.bancomarcela.models.mappers.TransactionMapper;
import com.desafio2.bancomarcela.models.repositories.AccountRepository;
import com.desafio2.bancomarcela.models.repositories.TransactionsRepository;
import com.desafio2.bancomarcela.models.repositories.UserRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AccountsService {

    private final TransactionsRepository transactionsRepository;
    private final AccountRepository accountRepository;
    private final UserRepository usersRepository;
    private final TransactionMapper transactionMapper;

    public AccountsService(TransactionsRepository transactionsRepository, AccountRepository accountRepository, UserRepository usersRepository,
                               TransactionMapper transactionMapper) {
        this.transactionsRepository = transactionsRepository;
        this.accountRepository = accountRepository;
        this.usersRepository = usersRepository;
        this.transactionMapper = transactionMapper;
    }


    public List<AccountEntity> findAllByUserid(String userid) {
        List<AccountEntity> accountEntity = accountRepository.findAllByUserid(userid);
        if (accountEntity.isEmpty()) {
            throw new NotFoundException("la transaction esta vacía");
        }
        return accountEntity;
    }

    public AccountEntity save(AccountEntity accountEntity) {
        List<AccountEntity> accountEntityBD = accountRepository.findAllByUserid(accountEntity.getUserid());

        if (!accountEntityBD.isEmpty())
        {
            throw new NotFoundException("La cuenta ya existe");
        }

       return accountRepository.save(accountEntity);

    }
}
