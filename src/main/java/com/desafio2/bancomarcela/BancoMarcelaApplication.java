package com.desafio2.bancomarcela;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoMarcelaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoMarcelaApplication.class, args);
	}

}
