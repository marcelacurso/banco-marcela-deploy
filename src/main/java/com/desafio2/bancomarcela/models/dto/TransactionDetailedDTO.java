package com.desafio2.bancomarcela.models.dto;

import com.desafio2.bancomarcela.models.enums.Currency;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class TransactionDetailedDTO {
    private String description;
    private BigDecimal amount;
    private Currency currency;
    private AccountDto from;
    private AccountDto to;
    private Date date;

    public TransactionDetailedDTO() {
    }

    public TransactionDetailedDTO(String description, BigDecimal amount, Currency currency, AccountDto from, AccountDto to,Date date ) {

        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
        this.date = date;
    }


    public String getDescription() {
        return description;
    }
    public BigDecimal getAmount() {

        return amount;
    }
    public Currency getCurrency() {

        return currency;
    }
    public AccountDto getFrom() {
        return from;
    }
    public AccountDto getTo() {

        return to;
    }
    public Date getDate()    {
        return date;
    }
}
