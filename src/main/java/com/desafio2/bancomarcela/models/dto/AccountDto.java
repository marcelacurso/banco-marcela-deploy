package com.desafio2.bancomarcela.models.dto;

public class AccountDto {
    private String firtsName;
    private String lastName;
    private String cbu;

    public String getFirtsName() {
        return firtsName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCbu() {
        return cbu;
    }

    public AccountDto(String firtsName, String lastName, String cbu) {
        this.firtsName = firtsName;
        this.lastName = lastName;
        this.cbu = cbu;
    }
}
