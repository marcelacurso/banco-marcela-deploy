package com.desafio2.bancomarcela.models.dto;


import javax.persistence.Column;
import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Date;

public class TransactionDTO{

    private Date date;
    private String description;
    private BigDecimal amount;
    private String currency;
    private String from;
    private String to;
    private String type;

    public TransactionDTO() {
    }


    public TransactionDTO(Date date, String description, BigDecimal amount, String currency, String from, String to, String type) {
        this.date = date;
        this.description = description;
        this.amount = amount;
        this.currency = currency;
        this.from = from;
        this.to = to;
        this.type = type;
    }
    public Date getDate() {
        return date;
 }
    public String getDescription() {
        return description;
    }
    public BigDecimal getAmount() {
        return amount;
    }
    public String getCurrency() {
        return currency;
    }
    public String getFrom() {
        return from;
    }
    public String getTo() {
        return to;
    }
    public String getType(){
        return type;
    }
}