package com.desafio2.bancomarcela.models.dto;
import com.desafio2.bancomarcela.models.enums.Currency;
import java.math.BigDecimal;

public class NewTransactionDTO {

    private String description;
    private BigDecimal amount;
    private Currency currency;
    private String from;
    private String to;

    public String getDescription() {
    return description;
    }
    public BigDecimal getAmount() {
    return amount;
    }
    public Currency getCurrency()
    {
        return currency;
    }

    public String getFrom() {
    return from;
    }
    public String getTo() {
    return to;
    }

}