package com.desafio2.bancomarcela.models.repositories;

import com.desafio2.bancomarcela.models.entities.AccountEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface AccountRepository extends CrudRepository<AccountEntity, Long> {
    List<AccountEntity> findAllByUserid(String userid);
    AccountEntity findByNumber(Long number);


    @GetMapping("/{userid}")
    default List<AccountEntity> getUserAccounts(@PathVariable String userid){

        return findAllByUserid(userid);
    }
}
