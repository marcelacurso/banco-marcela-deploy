package com.desafio2.bancomarcela.models.repositories;

import com.desafio2.bancomarcela.models.entities.TransactionsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

public interface TransactionsRepository extends CrudRepository<TransactionsEntity, Long> {

    List<TransactionsEntity> findAllByOriginOrDestination(String accountNumberOrigin, String accountNumberDest);

    Optional<TransactionsEntity> findById(Long id);
   }
