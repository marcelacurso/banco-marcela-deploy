package com.desafio2.bancomarcela.models.enums;
public enum AccountType {
    CUENTA_CORRIENTE, CAJA_DE_AHORROS
}
