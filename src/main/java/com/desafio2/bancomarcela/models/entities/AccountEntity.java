package com.desafio2.bancomarcela.models.entities;

import com.desafio2.bancomarcela.models.enums.AccountType;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity(name="accounts")

public class AccountEntity {
    @Id
    private Long number;
    private String cbu;
    @Enumerated(EnumType.STRING)
    private AccountType type;
    private String userid;

    public AccountEntity(){}

    public AccountEntity(Long number, String cbu, AccountType type, String userid ){
        this.number = number;
        this.cbu = cbu;
        this.type = type;
        this.userid = userid;
    }

    public Long getNumber() {
        return number;
    }

    public String getCbu() {

        return cbu;
    }

    public AccountType getType() {

        return type;
    }

    public String getUserid() {
        return userid;
    }

}
