package com.desafio2.bancomarcela.models.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="users")
public class UserEntity {
    @Id
    private String dni;
    private String firtname;
    private String lastname;
    private String adress;
    private String username;
    private String password;

    public UserEntity(){

    }

    public UserEntity(String dni, String firtname, String lastname, String adress, String username,String password){
        this.dni=dni;
        this.firtname=firtname;
        this.lastname=lastname;
        this.adress=adress;
        this.username = username;
        this.password = password;
    }

    public String getDni(){
        return dni;
    }

    public String getFirtName(){
        return firtname;
    }

    public String getLastName(){
        return lastname;
    }
    public String getAdress(){
        return adress;
    }

    public String fullName() {
        return firtname + " " + lastname;
    }
    public String getUsername(){
        return username;
    }
    public String getPassword(){
        return password;
    }

}
