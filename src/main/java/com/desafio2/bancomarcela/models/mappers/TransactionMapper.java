package com.desafio2.bancomarcela.models.mappers;

import com.desafio2.bancomarcela.models.dto.NewTransactionDTO;

import com.desafio2.bancomarcela.models.entities.TransactionsEntity;
import java.util.Date;
import org.springframework.stereotype.Component;

@Component
public class TransactionMapper {
    public TransactionsEntity mapTransactionDtoToTransactionEntity(NewTransactionDTO newTransactionDTO){
        return new TransactionsEntity(new Date(),
                newTransactionDTO.getDescription(),
                newTransactionDTO.getAmount(),
                newTransactionDTO.getCurrency(),
                newTransactionDTO.getFrom(),
                newTransactionDTO.getTo());
    }
}
